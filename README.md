# flutter_dogs

Application permettant d'afficher des photos de chiens en fonction de leur race

3 écrans :
- Accueil

	> Liste des races de chien
	

		- un clic = visualise une photo aléatoire de la race du chien séléctionné

		- un long press = ajoute aux favoris (le coeur ne passe pas en rouge)

- Favoris

	> Liste des races de chien en favoris
		- un clic = visualise une photo aléatoire de la race du chien séléctionné
		
		- un long press = supprime des favoris
- Aléatoire

	> Affiche une photo aléatoire d'une race aléatoire
	
		- un bouton pour reroll la photo

Utilisation de 3 API :
- Lister toutes les races de chiens
> https://dog.ceo/api/breed/{race_de_chien}/images/random

- Afficher la photo d'une race de chien en particulier
> https://dog.ceo/api/breeds/list/all

- Afficher une aléatoirement une photo d'une race de chien aléatoire
> https://dog.ceo/api/breeds/image/random
