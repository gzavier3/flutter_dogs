import 'dart:convert';

class AllDogBreed {
  final String breed;

  AllDogBreed({required this.breed});

  factory AllDogBreed.fromJson(String json) {
   Map<String, dynamic> map = jsonDecode(json);
    return AllDogBreed(
      breed: map['breed'],
    );
  }

  String toJson() {
    return jsonEncode({
      'breed': breed,
    });
  }
}