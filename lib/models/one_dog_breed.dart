class OneDogBreed {
  final String message;
  final String status;

  OneDogBreed({required this.message, required this.status});

  factory OneDogBreed.fromJson(Map<String, dynamic> json){
    return OneDogBreed(
      message: json['message'],
      status: json['status'],
    );
  }
}