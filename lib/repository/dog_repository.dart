import 'dart:convert';
import 'package:flutter_application/Models/all_dog_breed.dart';
import 'package:flutter_application/Models/one_dog_breed.dart';
import 'package:http/http.dart';

class DogRepository {
  Future<OneDogBreed> fetchOneDog(String breed) async {
    final Response response = await get(Uri.https(
      'dog.ceo',
      '/api/breed/$breed/images/random',
    ));
    if (response.statusCode == 200) {
      return OneDogBreed.fromJson(json.decode(response.body));
    } else {
      throw Exception('Impossible de charger la photo');
    }
  }

  Future<List<AllDogBreed>> fetchAllDogs() async {
    final Response response = await get(Uri.https(
      'dog.ceo',
      '/api/breeds/list/all',
    ));
    if (response.statusCode == 200) {
      final List<AllDogBreed> breeds = [];

      final Map<String, dynamic> json = jsonDecode(response.body);
      if (json.containsKey("message")) {
        final Map<String, dynamic> features = json['message'];
        for (var breed in features.keys) {
          breeds.add(AllDogBreed(breed: breed));
        }
      }
      return breeds;
    } else {
      throw Exception('Impossible de récuperer les races de chiens');
    }
  }

  Future<OneDogBreed> fetchRandomDogs() async {
    final Response response = await get(Uri.https(
      'dog.ceo',
      '/api/breeds/image/random',
    ));
    if (response.statusCode == 200) {
      return OneDogBreed.fromJson(json.decode(response.body));
    } else {
      throw Exception('Impossible de charger la photo');
    }
  }
}
