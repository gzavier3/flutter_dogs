import 'package:flutter_application/Models/all_dog_breed.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  Future<void> saveDogs(List<AllDogBreed> dogs) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final List<String> listJson = [];
    for (final AllDogBreed dog in dogs) {
      listJson.add(dog.toJson());
    }

    prefs.setStringList('dogs', listJson);
  }

  Future<List<AllDogBreed>> loadDogs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String>? list = prefs.getStringList('dogs');

    if(list == null) {
      return [];
    }

    final List<AllDogBreed> dogs = list.map((element) {
      return AllDogBreed.fromJson(element);
    }).toList();

    return dogs;
  }

}