import 'package:flutter_application/Models/all_dog_breed.dart';
import 'package:flutter_application/Models/one_dog_breed.dart';
import 'dog_repository.dart';

class Repository {
  Repository(this._dogRepository);

  final DogRepository _dogRepository;

  Future<OneDogBreed> fetchOneDogBreed(String breed) {
    return _dogRepository.fetchOneDog(breed);
  }

  Future<OneDogBreed> fetchRandomDog() {
    return _dogRepository.fetchRandomDogs();
  }

  Future<List<AllDogBreed>> fetchAllDogBreed() {
    return _dogRepository.fetchAllDogs();
  }
}
