import 'package:flutter/material.dart';
import 'package:flutter_application/ui/Screens/all_dogs.dart';
import 'package:flutter_application/ui/Screens/favorite.dart';
import 'package:flutter_application/ui/Screens/random.dart';

class MdsApp extends StatelessWidget {
  const MdsApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Photos de chiens',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      routes: {
        '/alldogs': (context) => AllDogs(),
        '/random': (context) => const Random(),
        '/favorite': (context) => Favorite(),
      },
      home: const MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int selectedPage = 0;

  final _pageOptions = [
    AllDogs(),
    Favorite(),
    const Random()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.orange,
        body: _pageOptions[selectedPage],
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home, size: 40), label : 'Accueil'),
            BottomNavigationBarItem(icon: Icon(Icons.favorite, size: 40), label : 'Favoris'),
            BottomNavigationBarItem(icon: Icon(Icons.wifi_protected_setup, size: 40), label : 'Aléatoire'),
          ],
          selectedItemColor: Colors.orange[900],
          elevation: 5.0,
          unselectedItemColor: Colors.orange,
          currentIndex: selectedPage,
          backgroundColor: Colors.grey[200],
          onTap: (index){
            setState(() {
              selectedPage = index;
            });
          },
        )
    );
  }
}
