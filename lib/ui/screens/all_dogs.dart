import 'package:flutter/material.dart';
import 'package:flutter_application/Models/all_dog_breed.dart';
import 'package:flutter_application/Repository/dog_repository.dart';
import 'package:flutter_application/Repository/repository.dart';
import 'package:flutter_application/repository/preferences_repository.dart';
import 'package:flutter_application/ui/Screens/one_dog.dart';

class AllDogs extends StatefulWidget {
  AllDogs({Key? key}) : super(key: key);
  final PreferencesRepository preferencesRepository = PreferencesRepository();

  @override
  State<AllDogs> createState() => _AllDogsState();
}

class _AllDogsState extends State<AllDogs> {
  List<AllDogBreed> savedDogs = [];

  final Repository repository = Repository(DogRepository());

  @override
  void initState() {
    widget.preferencesRepository.loadDogs().then((dogs) {
      _setDogs(dogs);
    });
    super.initState();
  }

  void _setDogs(List<AllDogBreed> dogs) {
    setState(() {
      savedDogs = dogs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Accueil'),
        centerTitle: true,
      ),
      body: Center(
          child: FutureBuilder(
              future: repository.fetchAllDogBreed(),
              builder: (context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (BuildContext context, int index) {
                        final AllDogBreed dog = snapshot.data[index];
                        bool isFavorite = savedDogs.contains(dog);
                        return ListTile(
                          title: Text(dog.breed),
                          leading: const Icon(Icons.pets),
                          trailing: Icon(
                              isFavorite
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              color: isFavorite ? Colors.red : null),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    OneDog(breed: dog.breed.toLowerCase()),
                              ),
                            );
                          },
                          onLongPress: () {
                            setState(() {
                              if (isFavorite) {
                                savedDogs.remove(dog);
                              } else {
                                savedDogs.add(dog);
                              }
                              widget.preferencesRepository
                                  .saveDogs(savedDogs);
                            });
                          },
                        );
                      });
                }
              })),
    );
  }
}
