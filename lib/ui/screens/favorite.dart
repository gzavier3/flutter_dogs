import 'package:flutter/material.dart';
import 'package:flutter_application/Models/all_dog_breed.dart';
import 'package:flutter_application/repository/preferences_repository.dart';

import 'one_dog.dart';

class Favorite extends StatefulWidget {
  Favorite({Key? key}) : super(key: key);

  final PreferencesRepository preferencesRepository = PreferencesRepository();

  @override
  State<Favorite> createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {

  List<AllDogBreed> _dogs = [];

  @override
  void initState() {
    widget.preferencesRepository.loadDogs().then((dogs) {
      _setDogs(dogs);
    });
    super.initState();
  }

  void _setDogs(List<AllDogBreed> dogs) {
    setState(() {
      _dogs = dogs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Favoris'),
        centerTitle: true,
      ),
      body: _dogs.isEmpty ? const Text("Pas de chiens en favoris", textAlign: TextAlign.center) : ListView.builder(
          itemCount: _dogs.length,
          itemBuilder: (context, index) {
            final AllDogBreed dog = _dogs[index];
            return ListTile(
              title: Text(dog.breed),
              leading: const Icon(Icons.pets),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        OneDog(breed: dog.breed.toLowerCase()),
                  ),
                );
              },
              onLongPress: () {
                setState(() {
                    _dogs.remove(dog);
                  widget.preferencesRepository
                      .saveDogs(_dogs);
                });
              },
            );
          }),
    );
  }
}