import 'package:flutter/material.dart';
import 'package:flutter_application/Models/one_dog_breed.dart';
import 'package:flutter_application/Repository/dog_repository.dart';
import 'package:flutter_application/Repository/repository.dart';

// ignore: must_be_immutable
class OneDog extends StatelessWidget {
  OneDog({Key? key, required this.breed}) : super(key: key);

  final String breed;
  final Repository repository = Repository(DogRepository());
  late Future<OneDogBreed> oneDog = repository.fetchOneDogBreed(breed);

  @override

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(breed),
      ),
      body: Center(
        child: FutureBuilder<OneDogBreed>(
          future: repository.fetchOneDogBreed(breed),
          builder: (context, snapshot){
            if(snapshot.hasData){
              return Image.network(snapshot.data!.message);
            }else if(snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}