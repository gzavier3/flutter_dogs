import 'package:flutter/material.dart';
import 'package:flutter_application/Models/one_dog_breed.dart';
import 'package:flutter_application/Repository/dog_repository.dart';
import 'package:flutter_application/Repository/repository.dart';

class Random extends StatefulWidget {
  const Random({Key? key}) : super(key: key);

  @override
  State<Random> createState() => _RandomState();
}

class _RandomState extends State<Random> {
  OneDogBreed? randomDog;
  final Repository repository = Repository(DogRepository());

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() {
    repository.fetchRandomDog().then((value) {
      setState(() {
        randomDog = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Images aléatoires'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          if (randomDog != null)
            Center(
              child: Image.network(randomDog!.message),
            ),
          if (randomDog == null)
            const Center(child: CircularProgressIndicator()),
          ElevatedButton(
              onPressed: () {
                loadData();
              },
              child: const Text('Regénerer'))
        ],
      ),
    );
  }
}
